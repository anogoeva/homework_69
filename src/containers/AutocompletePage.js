import React, {useEffect} from "react";
import {fetchMovieDetails, fetchMovies} from "../store/actions.js";
import {useDispatch, useSelector} from "react-redux";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const AutocompletePage = ({history}) => {
    const dispatch = useDispatch();
    const movies = useSelector(state => state.movies);

    useEffect(() => {
        dispatch(fetchMovies());
    }, [dispatch]);
    const movieDetails = useSelector(state => state.movieDetails);

    return (
        <>
            <p>Search for TV Show: </p>
            {movies && (
                <Autocomplete
                    onChange={(event, value) => {
                        history.replace('/shows/' + (value === null ? history.push('/') : value.show.id));
                        dispatch(fetchMovieDetails('/shows/' + (value === null ? history.push('/') : value.show.id)));
                    }}
                    id="combo-box-demo"
                    options={movies}
                    getOptionLabel={(option) => option.show.name}
                    style={{width: 300}}
                    renderInput={(params) => <TextField {...params} label="Combo box" variant="outlined"/>}
                />
            )}
            {movieDetails && (
                <div>
                    <p>{movieDetails.name}</p>
                    <img alt="movie" src={movieDetails.image.original} width="150" height="200"/>
                    <p>{movieDetails.language}</p>
                </div>
            )}
        </>
    );
};

export default AutocompletePage;