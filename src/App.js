import {Route, Switch} from "react-router-dom";
import AutocompletePage from "./containers/AutocompletePage";
import Layout from "./Components/Layout/Layout";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={AutocompletePage}/>
                <Route path="/shows/:id" component={AutocompletePage}/>
            </Switch>
        </Layout>
    )
};

export default App;
