import React from 'react';
import './NavigationItems.css';

const NavigationItems = () => {
    return (
        <ul className="NavigationItem">
            <li className="NavigationItem">
                <a href="/">TV shows</a>
            </li>
        </ul>
    );
};

export default NavigationItems;