import {
    FETCH_MOVIE_FAILURE,
    FETCH_MOVIE_REQUEST,
    FETCH_MOVIE_SUCCESS,
    FETCH_MOVIES_FAILURE,
    FETCH_MOVIES_REQUEST,
    FETCH_MOVIES_SUCCESS
} from "./actions";

const initialState = {
    fetchMoviesLoading: false,
    movies: null,
    onInputChange: null,
    movieDetails: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MOVIES_REQUEST:
            return {...state, fetchMoviesLoading: true};
        case FETCH_MOVIES_SUCCESS:
            return {...state, fetchMoviesLoading: false, movies: action.payload};
        case FETCH_MOVIES_FAILURE:
            return {...state, fetchMoviesLoading: false};
        case FETCH_MOVIE_REQUEST:
            return {...state, fetchMovieLoading: true};
        case FETCH_MOVIE_SUCCESS:
            return {...state, fetchMovieLoading: false, movieDetails: action.payload};
        case FETCH_MOVIE_FAILURE:
            return {...state, fetchMovieLoading: false};
        default:
            return state;
    }
};

export default reducer;