import axios from "axios";
import {apiURL} from "../config";

export const FETCH_MOVIES_REQUEST = 'FETCH_MOVIES_REQUEST';
export const FETCH_MOVIES_SUCCESS = 'FETCH_MOVIES_SUCCESS';
export const FETCH_MOVIES_FAILURE = 'FETCH_MOVIES_FAILURE';

export const FETCH_MOVIE_REQUEST = 'FETCH_MOVIE_REQUEST';
export const FETCH_MOVIE_SUCCESS = 'FETCH_MOVIE_SUCCESS';
export const FETCH_MOVIE_FAILURE = 'FETCH_MOVIE_FAILURE';

export const fetchMoviesRequest = () => ({type: FETCH_MOVIES_REQUEST});
export const fetchMoviesSuccess = movies => ({type: FETCH_MOVIES_SUCCESS, payload: movies});
export const fetchMoviesFailure = () => ({type: FETCH_MOVIES_FAILURE});

export const fetchMovieRequest = () => ({type: FETCH_MOVIE_REQUEST});
export const fetchMovieSuccess = movie => ({type: FETCH_MOVIE_SUCCESS, payload: movie});
export const fetchMovieFailure = () => ({type: FETCH_MOVIE_FAILURE});

export const fetchMovies = () => {
    return async dispatch => {
        try {
            dispatch(fetchMoviesRequest());
            const response = await axios.get(apiURL + '/search/shows?q=csi');
            dispatch(fetchMoviesSuccess(response.data));
        } catch (e) {
            dispatch(fetchMoviesFailure());
        }
    }
};

export const fetchMovieDetails = (location) => {
    return async dispatch => {
        try {
            dispatch(fetchMovieRequest());
            const response = await axios.get(apiURL + location);
            dispatch(fetchMovieSuccess(response.data));
        } catch (e) {
            dispatch(fetchMovieFailure());
        }
    }
};
